
#include <iostream>
#include "b/b.h"

using namespace std;

namespace b {
  void addIndentation(uint indentation) {
    for (uint i = 0; i < indentation; ++i) {
      std::cout << "  ";
    }
  }

  void doThingB(uint indentation) {
    addIndentation(indentation);
    cout << "B v1" << endl;
  };
}
